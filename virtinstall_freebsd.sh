virt-install \
    -n freebsd \
    -r 512 \
    --vcpus=2 \
    --os-variant=auto \
    --accelerate \
    -v \
    -c ./iso/FreeBSD-12.0-RELEASE-amd64-bootonly.iso \
    -w bridge:virbr0 \
    --vnc \
    --disk=pool=default,size=5,format=qcow2,bus=virtio

# --disk path=/raid10/kvm/freebsd73.img,size=4

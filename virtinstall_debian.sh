

virt-install --connect=qemu:///system \
             --location=http://ftp.us.debian.org/debian/dists/stable/main/installer\-amd64 \
             --initrd-inject=./preseed.cfg \
             --extra-args="auto" \
             --name d-i --ram=512 \
             -w bridge:virbr0 \
             --vcpus=2 \
             --os-variant=debian9 \
             --accelerate \
             --disk=pool=default,size=5,format=qcow2,bus=virtio

text
cdrom
auth --enableshadow --passalgo=sha512
keyboard --vckeymap=us --xlayouts='fr'
lang en_US.UTF-8
eula --agreed
reboot

network  --bootproto=dhcp --device=eth0 --ipv6=auto --activate
network  --hostname=centos1.lardiere.net
timezone Europe/Paris --isUtc

clearpart --none --initlabel
autopart --type=lvm --nohome --fstype=ext4
bootloader --location=mbr --boot-drive=vda

rootpw azerty
user --groups=wheel --name=loxodata --password=azerty --gecos="loxodata"

selinux --disabled
firewall --disabled

%packages --nobase --ignoremissing
@core
vim
bash-completion
sudo
wget
less
screen
sysstat
%end

%post --log=/root/post-install.log
echo 'Install PostgreSQL'
yum -y install https://download.postgresql.org/pub/repos/yum/11/redhat/rhel-7-x86_64/pgdg-centos11-11-2.noarch.rpm
yum -y install postgresql11 postgresql11-server postgresql11-contrib
%end

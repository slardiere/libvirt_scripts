
virt-install \
    -n centos7 \
    --ram 2048 \
    --vcpus=2 \
    --os-variant=centos7.0 \
    --accelerate \
    -v \
    -w bridge:virbr0 \
    --disk size=30,sparse=yes,format=qcow2 \
    -l http://mirror.centos.org/centos/7/os/x86_64/ \
    --graphics none \
    --console pty,target_type=serial \
    --initrd-inject=./rhel.ks \
    --extra-args="ks=file:/rhel.ks console=tty0 console=ttyS0,115200" \
